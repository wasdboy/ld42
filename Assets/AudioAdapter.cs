﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioAdapter : MonoBehaviour {

	void Start () 
	{
        Singleton = this;

        AudioController.Singleton.MusikAbspielen(clips[4],0.05f);
	}

    public static AudioAdapter Singleton;

    public AudioClip[] clips;

    public AudioClip[] zombieClips;
    public AudioClip[] schadenClips;
    public AudioClip[] teufelClips;

    public void GrabenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[0],0.2f);
    }

    public void SchleifenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[1],0.1f);
    }

    public void LaufenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[1],0.05f,0.7f);
    }

    public void TeufelSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(teufelClips[Random.Range(0, teufelClips.Length)], 0.2f, Random.Range(0.9f, 1.1f));
    }

    public void EngelSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[2],0.4f);
    }

    public void EngelErloestSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[5], 0.3f,Random.Range(1.05f,1.25f));
    }

    public void EngelZaehlenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[5], 0.15f, 1.9f);
    }

    public void GrabenFertigSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[3], 0.2f);
    }

    public void SchadenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(schadenClips[Random.Range(0, schadenClips.Length)], 0.3f, Random.Range(0.9f, 1.1f));
    }

    public void ZombieSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(zombieClips[Random.Range(0, zombieClips.Length)], 0.1f, Random.Range(0.9f, 1.1f));
    }

    public void HeilenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[2], 0.5f,0.8f);
    }

    public void ITrustGodSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[6], 0.5f, 1f);
    }

    public void SoulReleaseSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[7], 0.5f, 1f);
    }

    public void BrandstifterLachenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[9], 0.15f, 1.3f);
    }

    public void BrandstifterFeuerzeugSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[8], 0.2f, 1f);
    }

    public void FeuerLoeschenSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[10], 0.5f, 1f);
    }

    public void FeuerZuGlutSoundAbspielen()
    {
        AudioController.Singleton.SoundAbspielen(clips[11], 0.5f, 1f);
    }
}
