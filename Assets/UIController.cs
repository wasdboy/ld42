﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    private void Start()
    {
        Singleton = this;

        SpielerController.Singleton.HpChanged += UpdateHp;
        SpielerController.Singleton.AnzahlEngelChanged += UpdateEngel;

        UpdateHp();
        UpdateEngel();

        if (KonsitenteDaten.Singleton.Skill1Unlocked == false)
            Skill1Ui.NichtFreigeschaltet();
        if (KonsitenteDaten.Singleton.Skill2Unlocked == false)
            Skill2Ui.NichtFreigeschaltet();
        if (KonsitenteDaten.Singleton.Skill3Unlocked == false)
            Skill3Ui.NichtFreigeschaltet();
        if (KonsitenteDaten.Singleton.Skill4Unlocked == false)
            Skill4Ui.NichtFreigeschaltet();

        Skill1 = SchaufelSkill as ISkill;
        Skill2 = HeilenSkill as ISkill;
        Skill3 = ITrustGodSkill as ISkill;
        Skill4 = SoulReleaseSkill as ISkill;
    }

    public static UIController Singleton;
    public Image[] HpImages;
    public EngelPanelEnde EngelEndePanel;
    public Text EngelText;

    public SchaufelSkill SchaufelSkill;
    public HeilenSkill HeilenSkill;
    public SkillITrustGod ITrustGodSkill;
    public SkillSoulRelease SoulReleaseSkill;

    private ISkill Skill1;
    public SkillUI Skill1Ui;

    private ISkill Skill2;
    public SkillUI Skill2Ui;

    private ISkill Skill3;
    public SkillUI Skill3Ui;

    private ISkill Skill4;
    public SkillUI Skill4Ui;

    private void Update()
    {
        if (KonsitenteDaten.Singleton.Skill1Unlocked && Skill1.verfuegbar && Input.GetKeyUp(KeyCode.Alpha1))
        {
            Skill1.Benutzen();
            Skill1Ui.Benutzt();
        }

        if (KonsitenteDaten.Singleton.Skill2Unlocked && Skill2.verfuegbar && Input.GetKeyUp(KeyCode.Alpha2))
        {
            Skill2.Benutzen();
            Skill2Ui.Benutzt();
        }

        if (KonsitenteDaten.Singleton.Skill3Unlocked && Skill3.verfuegbar && Input.GetKeyUp(KeyCode.Alpha3))
        {
            Skill3.Benutzen();
            Skill3Ui.Benutzt();
        }

        if (KonsitenteDaten.Singleton.Skill4Unlocked && Skill4.verfuegbar && Input.GetKeyUp(KeyCode.Alpha4))
        {
            Skill4.Benutzen();
            Skill4Ui.Benutzt();
        }
    }

    public void UpdateHp()
    {
        var hp = SpielerController.Singleton.Hp;

        foreach (var hpImage in HpImages)
        {
            hpImage.color = Color.black;
        }

        for (int i = 0; i < hp; i++)
        {
            HpImages[i].color = Color.white;
        }
    }

    public void UpdateEngel()
    {
        EngelText.text = SpielerController.Singleton.AnzahlEngel.ToString();        
    }

    public void Restart()
    {
        KonsitenteDaten.Singleton.Speichern();
        SceneManager.LoadScene(1);
    }

    public void ZeigeEngelEnde()
    {
        foreach (var objekt in FindObjectsOfType<ZombieController>())
            Destroy(objekt.gameObject);

        foreach (var objekt in FindObjectsOfType<GrabController>())
            Destroy(objekt.gameObject);

        foreach (var objekt in FindObjectsOfType<LeichenSpawner>())
            Destroy(objekt.gameObject);

        foreach (var objekt in FindObjectsOfType<LeichenBringer>())
            Destroy(objekt.gameObject);

        foreach (var objekt in FindObjectsOfType<BrandstifterController>())
            Destroy(objekt.gameObject);

        foreach (var objekt in FindObjectsOfType<LeichenController>())
            Destroy(objekt.gameObject);

        EngelEndePanel.gameObject.SetActive(true);
        EngelEndePanel.Starte();
    }
}
