﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EngelPanelEnde : MonoBehaviour {

    public void Starte()
    {
        _engelHeute = SpielerController.Singleton.AnzahlEngel;
        EngelHeute.text = _engelHeute.ToString();
        EngelGesamt.text = KonsitenteDaten.Singleton.AnzahlEngleGesamt.ToString();
        _zaehleEngel = true;
    }

    public Text EngelHeute, EngelGesamt;
    public GameObject EndeButton;
    public GameObject GameOverScreen;
    public SkillUnlockPageController SkillUnlockPage;

    int _engelHeute;
    bool _zaehleEngel;
    bool _fertig;

    float _sleepTime = 2f;
    float _maximalesleepTime = 0.2f;

    private void Update()
    {
        if (_fertig)
            return;

        if (_engelHeute <= 0)
        { 
            _fertig = true;
            EndeButton.gameObject.SetActive(true);
            return;
        }

        if (_zaehleEngel == false)
            return;

        if (_sleepTime > 0)
        {
            _sleepTime -= Time.deltaTime;
            return;
        }

        if (_engelHeute > 0)
        {
            _engelHeute--;
            KonsitenteDaten.Singleton.AnzahlEngleGesamt++;
            EngelHeute.text = _engelHeute.ToString();
            EngelGesamt.text = KonsitenteDaten.Singleton.AnzahlEngleGesamt.ToString();
            AudioAdapter.Singleton.EngelZaehlenSoundAbspielen();
            _sleepTime = _maximalesleepTime;
        }
    }

    public void EndeButtonClicked()
    {
        KonsitenteDaten.Singleton.PruefeFreischaltungen();

        if (KonsitenteDaten.Singleton.NeueSkillInfos.Count <= 0)
        {
            GameOverScreen.SetActive(true);
        }
        else
        {
            SkillUnlockPage.gameObject.SetActive(true);
            var skillInfo = KonsitenteDaten.Singleton.NeueSkillInfos.First();
            KonsitenteDaten.Singleton.NeueSkillInfos.Remove(skillInfo);

            SkillUnlockPage.ZeigePanel(skillInfo);
        }
        Destroy(gameObject);
    }
}
