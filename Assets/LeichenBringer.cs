﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeichenBringer : MonoBehaviour {

	void Start () 
	{
        _animator = GetComponent<Animator>();
        _animator.SetTrigger(richtung);
	}

    Animator _animator;

    public Transform LeichenSpawnPosition;
    public LeichenController LeichePrefab;
    public SpriteRenderer SpriteRenderer;

    public string richtung;

    public void LeicheSpawnen()
    {
        Vector2 position = Vector2.zero;
        if (richtung == "links")
            position = new Vector2(LeichenSpawnPosition.position.x + Random.Range(-0.3f, 1f), LeichenSpawnPosition.position.y + Random.Range(-0.5f, 0.5f));
        if (richtung == "rechts")
            position = new Vector2(LeichenSpawnPosition.position.x + Random.Range(0.3f, -1f), LeichenSpawnPosition.position.y + Random.Range(-0.5f, 0.5f));
        if (richtung == "unten")
            position = new Vector2(LeichenSpawnPosition.position.x + Random.Range(-1f, 1f), LeichenSpawnPosition.position.y + Random.Range(-0.3f, 1f));

        Instantiate(LeichePrefab, position, Quaternion.identity);
    }

    public void Sterben()
    {
        Destroy(gameObject);
    }
}
