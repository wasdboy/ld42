﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Feuer : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var spieler = collision.gameObject.GetComponent<SpielerController>();
        if (spieler == null)
            return;
        spieler.SchadenNehmen(transform.position);
    }
}
