﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpielerAktionsHitbox : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var spielerAktion = collision.gameObject.GetComponent<ISpielerAktion>();

        if (spielerAktion == null)
            return;

        spielerAktion.IstAktiv = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var spielerAktion = collision.gameObject.GetComponent<ISpielerAktion>();

        if (spielerAktion == null)
            return;

        spielerAktion.IstAktiv = false;
    }
}
