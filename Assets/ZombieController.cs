﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour {

	void Start () 
	{
        _warteDauer = MaximaleWarteDauer;
        _verfolgungsdauer = MaximaleVerfolgungsdauer;
        _ziel = SpielerController.Singleton.transform;
        _rb2d = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _collider = GetComponent<BoxCollider2D>();

        _geraeuschWarteZeit = Random.Range(_minGeraueschWarteZeit, _maxGeraueschWarteZeit);
	}

    public enum EnumZombieStatus {Warten,SpielerVerfolgen }
    private enum EnumAnimationsStatus { Links, Rechts, Hoch, Runter, Idle }

    public Transform _ziel;

    public float BewegungsGeschwindigkeit;
    private float _warteDauer;
    public float MaximaleWarteDauer;
    private float _verfolgungsdauer;
    public float MaximaleVerfolgungsdauer;

    private EnumZombieStatus Status;
    private EnumAnimationsStatus AnimationsStatus;

    private Rigidbody2D _rb2d;
    private Animator _animator;
    private BoxCollider2D _collider;

    public SpriteRenderer SpriteRenderer;

    private float _minGeraueschWarteZeit = 5f;
    private float _maxGeraueschWarteZeit = 25f;
    private float _geraeuschWarteZeit;

    void FixedUpdate () 
	{
        if (_geraeuschWarteZeit > 0)
            _geraeuschWarteZeit -= Time.fixedDeltaTime;
        if (_geraeuschWarteZeit <= 0)
        {
            AudioAdapter.Singleton.ZombieSoundAbspielen();
            _geraeuschWarteZeit = Random.Range(_minGeraueschWarteZeit, _maxGeraueschWarteZeit);
        }

        switch (Status)
        {
            case EnumZombieStatus.Warten:
                WartenUpdate();
                break;
            case EnumZombieStatus.SpielerVerfolgen:
                VerfolgenUpdate();
                break;
            default:
                break;
        }

        BlickRichtung();
    }

    void BlickRichtung()
    {
        if (_rb2d.velocity == Vector2.zero && AnimationsStatus != EnumAnimationsStatus.Idle)
        {
            _animator.SetFloat("Speed", 0);
            AnimationsStatus = EnumAnimationsStatus.Idle;
            return;
        }

        if (_rb2d.velocity == Vector2.zero && AnimationsStatus == EnumAnimationsStatus.Idle)
        {
            return;
        }

        var x = _rb2d.velocity.x;
        var y = _rb2d.velocity.y;

        _animator.SetFloat("Speed", 1);

        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (x > 0 && AnimationsStatus != EnumAnimationsStatus.Rechts)
            {
                AnimationsStatus = EnumAnimationsStatus.Rechts;
                _animator.SetTrigger("Rechts");
                return;
            }
            if (x < 0 && AnimationsStatus != EnumAnimationsStatus.Links)
            {
                AnimationsStatus = EnumAnimationsStatus.Links;
                _animator.SetTrigger("Links");
                return;
            }
        }
        else
        {
            if (y > 0 && AnimationsStatus != EnumAnimationsStatus.Hoch)
            {
                AnimationsStatus = EnumAnimationsStatus.Hoch;
                _animator.SetTrigger("Hoch");
                return;
            }
            if (y < 0 && AnimationsStatus != EnumAnimationsStatus.Runter)
            {
                AnimationsStatus = EnumAnimationsStatus.Runter;
                _animator.SetTrigger("Runter");
                return;
            }
        }
    }

    void WartenUpdate()
    {
        if (_warteDauer > 0)
        {
            _warteDauer -= Time.fixedDeltaTime;
            return;
        }

        AudioAdapter.Singleton.ZombieSoundAbspielen();
        Status = EnumZombieStatus.SpielerVerfolgen;
        _warteDauer = MaximaleWarteDauer;
    }

    void VerfolgenUpdate()
    {
        var richtung = _ziel.position - transform.position;
        _rb2d.velocity = richtung.normalized * BewegungsGeschwindigkeit * Time.fixedDeltaTime;
        _verfolgungsdauer -= Time.fixedDeltaTime;

        if (_verfolgungsdauer > 0)
            return;

        VerfolgenFertig();
    }

    void VerfolgenFertig()
    {
        _verfolgungsdauer = MaximaleVerfolgungsdauer;
        _rb2d.velocity = Vector2.zero;
        Status = EnumZombieStatus.Warten;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var spieler = collision.gameObject.GetComponent<SpielerController>();
        if (spieler == null)
            return;

        VerfolgenFertig();
        spieler.SchadenNehmen(transform.position);
    }

    public void Erloesen()
    {
        StopAllCoroutines();
        _animator.SetTrigger("ITrustGod");
        _collider.enabled = false;
    }

    public void ZombieTod()
    {
        StartCoroutine(Sterben());        
    }

    private IEnumerator Sterben()
    {
        yield return new WaitForSeconds(Random.Range(0.05f, 0.3f));

        AudioAdapter.Singleton.EngelErloestSoundAbspielen();
        SpielerController.Singleton.AnzahlEngel++;
        Destroy(gameObject);
    }

    public IEnumerator FlasheFarbe(Color farbe, float dauer, int anzahl)
    {
        for (int i = 0; i < anzahl; i++)
        {
            SpriteRenderer.color = farbe;
            yield return new WaitForSeconds(dauer);
            SpriteRenderer.color = Color.white;
            yield return new WaitForSeconds(dauer);
        }
    }
}
