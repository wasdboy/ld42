﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LeichenSpawner : MonoBehaviour {

    private void Start()
    {
        _anzahlLeichen = 0;
        _anzahlZombies = 0;

        _spawnCoolDownBrandstfiter = 10;

        _graeber = FindObjectsOfType<GrabController>();
    }

    public GameObject LeichenBringerPrefab;
    public GameObject BrandStiferPrefab;

    public Transform[] LeichenBringerSpawnPosition;
    public Transform[] LeichenSpawnPosition;

    public Sprite LinksSprite;
    public Sprite RechtsSprite;
    public Sprite UntenSprite;

    private GrabController[] _graeber;

    private static int _anzahlZombies;
    private static int _anzahlLeichen;

    private float _spawnCoolDownLeichenBringer;
    private float _maxSpawnCoolDownLeichenBringer = 10f;
    private float _minSpawnCoolDownLeichenBringer = 8f;
    private float _minSpawnCoolDownLeichenBringerAbsolut = 1f;

    private float _spawnCoolDownBrandstfiter;
    private float _maxSpawnCoolDownBrandstfiter = 20f;
    private float _minSpawnCoolDownBrandstfiter = 15f;
    private float _minSpawnCoolDownBrandstfiterAbsolut = 12f;

    void Update () 
	{
        LeichenBringerSpawnen();
        BrandstfiterSpawnen();
	}

    void BrandstfiterSpawnen()
    {
        if (_spawnCoolDownBrandstfiter > 0)
        {
            _spawnCoolDownBrandstfiter -= Time.deltaTime;
            return;
        }

        BrandstifterBringen();

        _maxSpawnCoolDownBrandstfiter -= 0.1f;
        if (_maxSpawnCoolDownBrandstfiter < _minSpawnCoolDownBrandstfiter)
            _maxSpawnCoolDownBrandstfiter = _minSpawnCoolDownBrandstfiter;

        _minSpawnCoolDownBrandstfiter -= 0.1f;
        if (_minSpawnCoolDownBrandstfiter < _minSpawnCoolDownBrandstfiterAbsolut)
            _minSpawnCoolDownBrandstfiter = _minSpawnCoolDownBrandstfiterAbsolut;

        _spawnCoolDownBrandstfiter = Random.Range(_minSpawnCoolDownBrandstfiter, _maxSpawnCoolDownBrandstfiter);

        ZuVieleOffeneGraeberStrafe();
    }

    void ZuVieleOffeneGraeberStrafe()
    {
        var anzahlOffeneGraeber = _graeber.Count(p => p.Status == GrabController.EnumGrabStatus.Offen);

        if (anzahlOffeneGraeber <= 3)
            return;

        var multiplikator = anzahlOffeneGraeber * 1.3f;

        _spawnCoolDownBrandstfiter -= multiplikator;

        if (_spawnCoolDownBrandstfiter < 1)
            _spawnCoolDownBrandstfiter = 1;
    }

    void LeichenBringerSpawnen()
    {
        if (_spawnCoolDownLeichenBringer > 0)
        {
            _spawnCoolDownLeichenBringer -= Time.deltaTime;
            return;
        }

        LeicheBringen();

        _maxSpawnCoolDownLeichenBringer -= 0.1f;
        if (_maxSpawnCoolDownLeichenBringer < _minSpawnCoolDownLeichenBringer)
            _maxSpawnCoolDownLeichenBringer = _minSpawnCoolDownLeichenBringer;

        _minSpawnCoolDownLeichenBringer -= 0.1f;
        if (_minSpawnCoolDownLeichenBringer < _minSpawnCoolDownLeichenBringerAbsolut)
            _minSpawnCoolDownLeichenBringer = _minSpawnCoolDownLeichenBringerAbsolut;

        _anzahlLeichen++;
        _spawnCoolDownLeichenBringer = Random.Range(_minSpawnCoolDownLeichenBringer, _maxSpawnCoolDownLeichenBringer);
    }

    public void BrandstifterBringen()
    {
        var zufallsZahl = Random.Range(0, 6);
        Instantiate(BrandStiferPrefab, LeichenBringerSpawnPosition[zufallsZahl].position, Quaternion.identity).GetComponentInChildren<BrandstifterController>();
    }

    public void LeicheBringen()
    {
        var zufallsZahl = Random.Range(0, 6);

        string richtung = "";
        Sprite sprite = new Sprite();

        if (zufallsZahl == 0 || zufallsZahl == 1)
        {
            richtung = "links";
            sprite = LinksSprite;
        }
        if (zufallsZahl == 2 || zufallsZahl == 3 || zufallsZahl == 5)
        {
            richtung = "rechts";
            sprite = RechtsSprite;
        }
        if (zufallsZahl == 4)
        {
            richtung = "unten";
            sprite = UntenSprite;
        }
        var bringerSpawnPunkt = LeichenBringerSpawnPosition[zufallsZahl];
        var leichenSpawnPunkt = LeichenSpawnPosition[zufallsZahl];

        var leichenBringer = Instantiate(LeichenBringerPrefab, bringerSpawnPunkt.position, Quaternion.identity).GetComponentInChildren<LeichenBringer>();

        leichenBringer.richtung = richtung;
        leichenBringer.LeichenSpawnPosition = leichenSpawnPunkt;
        leichenBringer.SpriteRenderer.sprite = sprite;
    }

    public static void NeuerZombie(ZombieController zombie)
    {
        zombie.BewegungsGeschwindigkeit += (_anzahlZombies * 5);
        zombie.MaximaleVerfolgungsdauer += (_anzahlZombies * 0.3f);
        zombie.MaximaleWarteDauer -= (_anzahlZombies * 0.1f);

        AudioAdapter.Singleton.ZombieSoundAbspielen();

        _anzahlZombies++;
    }

    public static void NeuerZombieRot(ZombieController zombie)
    {
        zombie.BewegungsGeschwindigkeit += (_anzahlZombies * 3);
        zombie.MaximaleVerfolgungsdauer += (_anzahlZombies * 0.5f);
        zombie.MaximaleWarteDauer -= (_anzahlZombies * 0.3f);

        AudioAdapter.Singleton.ZombieSoundAbspielen();

        _anzahlZombies++;
    }
}
