﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpielerAnimationsHelper : MonoBehaviour {

    public void StartITrustGod()
    {
        AudioAdapter.Singleton.ITrustGodSoundAbspielen();
        _zombies = new List<ZombieController>();
        SpielerController.Singleton.StartCoroutine(SpielerController.Singleton.FlasheFarbe(Color.cyan, 0.1f, 17));
    }

    CircleCollider2D _hitBox;

    private List<ZombieController> _zombies;

    public void ZombieHinzufuegen(ZombieController zombie)
    {
        if (_zombies.Contains(zombie) == false)
            _zombies.Add(zombie);

         zombie.StartCoroutine(zombie.FlasheFarbe(Color.cyan, 0.1f, 60));
    }

    public void MonsterToeten()
    {
        while (_zombies.Count > 0)
        {
            var zombie = _zombies[0];
            zombie.Erloesen();
            _zombies.Remove(zombie);
        }
    }

    public void EndeITrustGod()
    {
        SpielerController.Singleton.EndeITrustGod();
    }
}
