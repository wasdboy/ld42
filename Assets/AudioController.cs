﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AudioController : MonoBehaviour {

    public static AudioController Singleton;

    private List<AudioSource> _playersSound;

    void Start () 
	{
        if (Singleton == null)
            Singleton = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        _playersSound = new List<AudioSource>();
	}

    public void SoundAbspielen(AudioClip sound, float volume = 1f, float pitch = 1f)
    {
        if (sound == null)
            throw new System.Exception("Audioclip ist null");

        var player = GetFreierPlayer();

        player.volume = volume;
        player.pitch = pitch;

        player.loop = false;

        player.clip = sound;
        player.Play();
    }

    public void MusikAbspielen(AudioClip sound, float volume = 1f, float pitch = 1f)
    {
        if (sound == null)
            throw new System.Exception("Audioclip ist null");

        var player = GetFreierPlayer();

        player.volume = volume;
        player.pitch = pitch;
        player.loop = true;

        player.clip = sound;
        player.Play();
    }

    private AudioSource GetFreierPlayer()
    {
        var player = _playersSound.FirstOrDefault(p => p.isPlaying == false);

        if (player == null)
        {
            player = gameObject.AddComponent<AudioSource>() as AudioSource;

            player.loop = false;
            player.playOnAwake = false;

            _playersSound.Add(player);
        }

        return player;
    }
}
