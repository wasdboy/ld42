﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabAnimationsController : MonoBehaviour {

    public GrabController Grabcontroller;

    public void FeuerAn()
    {
        Grabcontroller.FeuerAn();
        Grabcontroller.ZombieSpawnen();
    }

    public void EngelInHimmel()
    {
        Grabcontroller.EngelInHimmel();
    }
}
