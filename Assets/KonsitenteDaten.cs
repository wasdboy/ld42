﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KonsitenteDaten : MonoBehaviour {

    private void Awake()
    {
        Singleton = this;
        AnzahlEngleGesamt = PlayerPrefs.GetInt("AnzahlEngelGesamt");
        Skill1Unlocked = PlayerPrefs.GetInt("Skill1Unlocked") == 1;
        Skill2Unlocked = PlayerPrefs.GetInt("Skill2Unlocked") == 1;
        Skill3Unlocked = PlayerPrefs.GetInt("Skill3Unlocked") == 1;
        Skill4Unlocked = PlayerPrefs.GetInt("Skill4Unlocked") == 1;
    }

    public static KonsitenteDaten Singleton;

    public int AnzahlEngleGesamt;
    public bool Skill1Unlocked;
    public bool Skill2Unlocked;
    public bool Skill3Unlocked;
    public bool Skill4Unlocked;

    private int _freischaltenSkill1 = 1;
    private int _freischaltenSkill2 = 10;
    private int _freischaltenSkill3 = 30;
    private int _freischaltenSkill4 = 75;

    public List<NeuerSkillInfo> NeueSkillInfos = new List<NeuerSkillInfo>();

    public NeuerSkillInfo SkillInfo1;
    public NeuerSkillInfo SkillInfo2;
    public NeuerSkillInfo SkillInfo3;
    public NeuerSkillInfo SkillInfo4;

    public void Speichern()
    {
        PlayerPrefs.SetInt("AnzahlEngelGesamt",AnzahlEngleGesamt);
        PlayerPrefs.SetInt("Skill1Unlocked", Skill1Unlocked ? 1 : 0);
        PlayerPrefs.SetInt("Skill2Unlocked", Skill2Unlocked ? 1 : 0);
        PlayerPrefs.SetInt("Skill3Unlocked", Skill3Unlocked ? 1 : 0);
        PlayerPrefs.SetInt("Skill4Unlocked", Skill4Unlocked ? 1 : 0);
    }

    public void PruefeFreischaltungen()
    {
        if (Skill1Unlocked == false && AnzahlEngleGesamt >= _freischaltenSkill1)
        {
            Skill1Unlocked = true;
            NeueSkillInfos.Add(SkillInfo1);
        }
        if (Skill2Unlocked == false && AnzahlEngleGesamt >= _freischaltenSkill2)
        {
            Skill2Unlocked = true;
            NeueSkillInfos.Add(SkillInfo2);
        }
        if (Skill3Unlocked == false && AnzahlEngleGesamt >= _freischaltenSkill3)
        {
            Skill3Unlocked = true;
            NeueSkillInfos.Add(SkillInfo3);
        }
        if (Skill4Unlocked == false && AnzahlEngleGesamt >= _freischaltenSkill4)
        {
            Skill4Unlocked = true;
            NeueSkillInfos.Add(SkillInfo4);
        }
        Speichern();
    }
}
