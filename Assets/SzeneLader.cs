﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SzeneLader : MonoBehaviour {

    public Image Image;
    public Text Text;

    public void OnEnter()
    {
        Image.color = Color.red;
        Text.color = Color.red;
    }

    public void OnLeave()
    {
        Image.color = Color.white;
        Text.color = Color.white;
    }

    public void LadeSzene(int index)
    {
        SceneManager.LoadScene(index);
    }
}
