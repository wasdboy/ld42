﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpielerController : MonoBehaviour {

    void Start()
    {
        Singleton = this;

        _rb2d = GetComponent<Rigidbody2D>();
        _animator = GetComponentInChildren<Animator>();
        _bewegungsgeschwindigkeit = BewegungStandard;
        Links = -11.03f;
        Rechts = 10.6f;
        Oben = 4.56f;
        Unten = -4.95f;

        Hp = 3;
    }

    public enum EnumSpielerStatus { FreiFuerAktion, Busy }
    private enum EnumAnimationsStatus { Links, Rechts, Hoch, Runter, Idle }

    public static SpielerController Singleton;

    public EnumSpielerStatus Status;
    private EnumAnimationsStatus AnimationsStatus;

    public Action SpielerAktion;
    public Vector2 Velocity
    {
        get { return _rb2d.velocity; }
    }
    public SpriteRenderer _renderer;
    public LeichenController LeichenController;
    public event Action HpChanged;
    public event Action AnzahlEngelChanged;

    Vector2 _eingabeRichtung;
    Rigidbody2D _rb2d;
    Animator _animator;

    float _bewegungsgeschwindigkeit;
    readonly float BewegungStandard = 210;
    readonly float BewegungTragend = 125f;

    public float Links { get; private set; }
    public float Rechts { get; private set; }
    public float Unten { get; private set; }
    public float Oben { get; private set; }

    public bool IstImmun;
    private bool KannNichtsMachen;

    private int _anzahlEngel { get; set; }
    public int AnzahlEngel
    {
        get { return _anzahlEngel; }
        set
        {
            _anzahlEngel = value;
            if (AnzahlEngelChanged != null)
                AnzahlEngelChanged();
        }
    }

    private int _hp;
    public int Hp
    {
        get { return _hp; }
        set
        {
            _hp = value;
            if (HpChanged != null)
                HpChanged();
        }
    }

    private float _laufenSoundTimer;
    private float _maximalLaufenSoundTimer = 0.3f;
    private float _bewegungBlockiertTimer;

    void Update () 
	{
        if (Hp <= 0 || KannNichtsMachen)
            return;

        if (_rb2d.velocity == Vector2.zero && AnimationsStatus != EnumAnimationsStatus.Idle)
        {
            AnimationsStatus = EnumAnimationsStatus.Idle;
            _animator.SetFloat("Speed", 0);
        }

        if (_bewegungBlockiertTimer > 0)
        {
            _bewegungBlockiertTimer -= Time.deltaTime;
            return;
        }

        _eingabeRichtung = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        BlickrichtungUpdate();

        if (Input.GetKeyUp(KeyCode.Space) && SpielerAktion != null)
            SpielerAktion();

        if (_rb2d.velocity != Vector2.zero)
            LaufenSoundAbspielen();

        var x = transform.position.x;
        var y = transform.position.y;

        if (transform.position.x < Links)
            x = Links;
        if (transform.position.x > Rechts)
            x = Rechts;
        if (transform.position.y < Unten)
            y = Unten;
        if (transform.position.y > Oben)
            y = Oben;

        transform.position = new Vector2(x, y);
    }

    private void BlickrichtungUpdate()
    {
        if (_eingabeRichtung == Vector2.zero && AnimationsStatus == EnumAnimationsStatus.Idle)
            return;

        var x = _eingabeRichtung.x;
        var y = _eingabeRichtung.y;

        _animator.SetFloat("Speed", 1);

        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (x > 0 && AnimationsStatus != EnumAnimationsStatus.Rechts)
            {
                AnimationsStatus = EnumAnimationsStatus.Rechts;
                _animator.SetTrigger("Rechts");
                return;
            }
            if (x < 0 && AnimationsStatus != EnumAnimationsStatus.Links)
            {
                AnimationsStatus = EnumAnimationsStatus.Links;
                _animator.SetTrigger("Links");
                return;
            }
        }
        else
        {
            if (y > 0 && AnimationsStatus != EnumAnimationsStatus.Hoch)
            {
                AnimationsStatus = EnumAnimationsStatus.Hoch;
                _animator.SetTrigger("Hoch");
                return;
            }
            if (y < 0 && AnimationsStatus != EnumAnimationsStatus.Runter)
            {
                AnimationsStatus = EnumAnimationsStatus.Runter;
                _animator.SetTrigger("Runter");
                return;
            }
        }
    }

    private void FixedUpdate()
    {
        if (_bewegungBlockiertTimer > 0)
            return;

        _rb2d.velocity = _eingabeRichtung.normalized * (_bewegungsgeschwindigkeit * Time.fixedDeltaTime);
    }

    public void SetBewegungNormal()
    {
        _bewegungsgeschwindigkeit = BewegungStandard;
    }

    public void SetBewegungTragend()
    {
        _bewegungsgeschwindigkeit = BewegungTragend;
    }

    void LaufenSoundAbspielen()
    {
        if (_laufenSoundTimer > 0)
        {
            _laufenSoundTimer -= Time.deltaTime;
            return;
        }

        if(_bewegungsgeschwindigkeit == BewegungTragend)
            AudioAdapter.Singleton.SchleifenSoundAbspielen();

        if (_bewegungsgeschwindigkeit == BewegungStandard)
            AudioAdapter.Singleton.LaufenSoundAbspielen();

        _laufenSoundTimer = _maximalLaufenSoundTimer;

    }

    public void SchadenNehmen(Vector2 verursacherPosition)
    {
        var richtung = (Vector2)transform.position - verursacherPosition;

        _rb2d.velocity = Vector2.zero;
        _eingabeRichtung = Vector2.zero;
        _rb2d.AddForce(richtung.normalized * 500f);
        _bewegungBlockiertTimer = 0.1f;

        if (_bewegungsgeschwindigkeit == BewegungTragend && LeichenController != null)
            LeichenController.StoppeTragen();

        if (IstImmun)
            return;

        Hp--;
        FlasheFarbe(Color.red, 0.2f, 1);
        AudioAdapter.Singleton.SchadenSoundAbspielen();

        if (Hp <= 0)
        {
            UIController.Singleton.ZeigeEngelEnde();
        }
    }

    public void Grabe(Transform positon)
    {
        Status = EnumSpielerStatus.Busy;
        StartCoroutine(AsyncGrabe(positon));
    }

    public void StartITrustGod()
    {
        _animator.SetTrigger("ITrustGod");
        IstImmun = true;
        KannNichtsMachen = true;
        _rb2d.isKinematic = true;
        _rb2d.velocity = Vector2.zero;
        _eingabeRichtung = Vector2.zero;
    }

    public void EndeITrustGod()
    {
        _rb2d.isKinematic = false;
        _animator.SetTrigger("Runter");
        _animator.SetFloat("Speed",0);
        IstImmun = false;
        KannNichtsMachen = false;        
        _rb2d.velocity = Vector2.zero;
        _eingabeRichtung = Vector2.zero;
    }

    private IEnumerator AsyncGrabe(Transform position)
    {
        _eingabeRichtung = Vector2.zero;
        _rb2d.velocity = Vector2.zero;

        _bewegungBlockiertTimer = 0.3f;

        var originalPostion = transform.position;

        var richtung = transform.position - position.position;
        richtung = (Vector2)richtung.normalized;

        var richtungString = "";
        if (Mathf.Abs(richtung.x) > Math.Abs(richtung.y))
            richtungString = richtung.x > 0? "Links" : "Rechts";
        else
            richtungString = richtung.y > 0 ? "Runter" : "Hoch";

        _animator.SetTrigger(richtungString);

        var warteZeitHin = 0.15f;
        while (warteZeitHin > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, position.position, 4f * Time.deltaTime);
            warteZeitHin -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        var warteZeitZurueck = 0.15f;
        while (warteZeitZurueck > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, originalPostion, 3.5f * Time.deltaTime);
            warteZeitZurueck -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        Status = EnumSpielerStatus.FreiFuerAktion;
    }

    public IEnumerator FlasheFarbe(Color farbe, float dauer, int anzahl )
    {
        for (int i = 0; i < anzahl; i++)
        {
            _renderer.color = farbe;
            yield return new WaitForSeconds(dauer);
            _renderer.color = Color.white;
            yield return new WaitForSeconds(dauer);
        }
    }
}
