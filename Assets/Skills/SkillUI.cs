﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillUI : MonoBehaviour {

    void Start()
    {
        _image = GetComponent<Image>();
    }

    Image _image;

    public Image SkillImage;
    public Sprite NichtFreigeschaltetSprite;

    public Color BGFarbeNichtFreigeschlatet;
    public Color BGFarbeVerfuegbar;
    public Color BGFarbeBenutzt;

    public void NichtFreigeschaltet()
    {
        SkillImage.sprite = NichtFreigeschaltetSprite;
        _image.color = BGFarbeNichtFreigeschlatet;
    }

    public void Benutzt()
    {
        _image.color = BGFarbeBenutzt;
    }
}
