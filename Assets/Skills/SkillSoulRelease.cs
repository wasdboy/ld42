﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SkillSoulRelease : MonoBehaviour,ISkill {

    private void Start()
    {
        verfuegbar = true;
    }

    public bool verfuegbar { get; set; }

    public void Benutzen()
    {
        verfuegbar = false;

        StartCoroutine(SpielerController.Singleton.FlasheFarbe(Color.cyan, 0.1f, 5));
        AudioAdapter.Singleton.SoulReleaseSoundAbspielen();
        StartCoroutine(Erloesen());
    }

    private IEnumerator Erloesen()
    {
        var leichen = FindObjectsOfType<LeichenController>()
            .ToList();

        foreach (var leiche in leichen)
        {
            if (leiche == null)
                continue;

            if (leiche.IstBegraben)
                leiche.ErloesenImGrab();
            else
                leiche.Erloesen();
            yield return new WaitForSeconds(Random.Range(0.1f, 0.25f));
        }

        var zombies = FindObjectsOfType<ZombieController>()
            .ToList();

        foreach (var zombie in zombies)
        {
            zombie.Erloesen();
            yield return new WaitForSeconds(Random.Range(0.1f, 0.25f));
        }
    }
}
