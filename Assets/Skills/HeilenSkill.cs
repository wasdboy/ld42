﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeilenSkill : MonoBehaviour,ISkill {


    private void Start()
    {
        verfuegbar = true;
    }

    public bool verfuegbar { get; set; }

    public void Benutzen()
    {
        verfuegbar = false;

        SpielerController.Singleton.IstImmun = true;

        StartCoroutine(SpielerController.Singleton.FlasheFarbe(Color.green, 0.15f, 3));
        AudioAdapter.Singleton.HeilenSoundAbspielen();

        SpielerController.Singleton.Hp = 3;
        StartCoroutine(StoppeImmunitaet());
    }

    private IEnumerator StoppeImmunitaet()
    {
        yield return new WaitForSeconds(0.5f);

        SpielerController.Singleton.IstImmun = false;
    }
}
