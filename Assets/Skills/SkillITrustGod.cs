﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillITrustGod : MonoBehaviour,ISkill {

    private void Start()
    {
        verfuegbar = true;
    }

    public bool verfuegbar { get; set; }

    public void Benutzen()
    {
        verfuegbar = false;

        SpielerController.Singleton.StartITrustGod();
    }
}
