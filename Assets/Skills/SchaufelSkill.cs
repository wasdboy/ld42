﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SchaufelSkill : MonoBehaviour, ISkill
{
    private void Start()
    {
        verfuegbar = true;
    }

    public bool verfuegbar { get; set ; }

    public void Benutzen()
    {
        verfuegbar = false;

        var graeber = FindObjectsOfType<GrabController>()
            .Where(p => p.Leiche == null && p.Status == GrabController.EnumGrabStatus.Geschlossen)
            .OrderBy(p => Mathf.Abs(Vector2.Distance(p.transform.position, SpielerController.Singleton.transform.position)))
            .ToList();
        int counter = 0;

        if(graeber.Count() > 0)
            AudioAdapter.Singleton.GrabenFertigSoundAbspielen();

        foreach (var grab in graeber)
        {
            if (counter >= 5)
                break;

            grab.GrabOeffnenSkill();

            counter++;
        }
    }
}
