﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BrandstifterController : MonoBehaviour {

	void Start () 
	{
        _start = transform.position;
        _rb2d = GetComponent<Rigidbody2D>();
        AnimationsStatus = EnumAnimationsStatus.Idle;
        ZielSuchen();
	}

    public Rigidbody2D _rb2d;
    public Animator _animator;
    public GameObject Kreis;

    private GrabController meinGrab;
    private Vector3 _ziel;
    private Vector3 _start;

    private float _laufgeschwindigkeit = 200f;

    private enum EnumAnimationsStatus { Links, Rechts, Hoch, Runter, Idle }
    private enum EnumBrandstifterStatus { LaufeZuZiel, LegeFeuer, FeuerAmLegen, GeheHeim}
    private EnumAnimationsStatus AnimationsStatus;
    private EnumBrandstifterStatus Status;

    void Update () 
	{
        BlickRichtung();

        if (Status == EnumBrandstifterStatus.GeheHeim && Mathf.Abs(Vector2.Distance(transform.position, _ziel)) <= 0.1f)
            Destroy(gameObject);

        if (Status == EnumBrandstifterStatus.LaufeZuZiel && Mathf.Abs(Vector2.Distance( transform.position ,_ziel)) <= 0.1f)
            StartCoroutine(FeuerLegen());

        if (Status != EnumBrandstifterStatus.LaufeZuZiel && Status != EnumBrandstifterStatus.GeheHeim)
            return;

        var richtung = _ziel - transform.position;
        _rb2d.velocity = richtung.normalized * _laufgeschwindigkeit * Time.fixedDeltaTime;        
    }

    IEnumerator FeuerLegen()
    {
        Status = EnumBrandstifterStatus.LegeFeuer;

        _rb2d.velocity = Vector2.zero;


        Kreis.SetActive(true);
        AudioAdapter.Singleton.BrandstifterFeuerzeugSoundAbspielen();

        yield return new WaitForSeconds(0.2f);
        Kreis.SetActive(false);

        yield return new WaitForSeconds(0.3f);
        meinGrab.GlutLegen();

        yield return new WaitForSeconds(0.5f);

        AudioAdapter.Singleton.BrandstifterLachenSoundAbspielen();

        _laufgeschwindigkeit = 300f;
        
        _ziel = _start;
        Status = EnumBrandstifterStatus.GeheHeim;
    }

    void ZielSuchen()
    {
        var graeber = FindObjectsOfType<GrabController>()
            .Where(p => p.Glimmt == false && p.Status != GrabController.EnumGrabStatus.Zerstoert)
            .OrderByDescending(p => Vector2.Distance(transform.position, p.transform.position))
            .Take(5)
            .ToList();

        var grab = graeber[Random.Range(0, graeber.Count)];

        if (grab == null)
        {
            Debug.LogError("Kein grab da?");
            return;
        }

        meinGrab = grab;

        _ziel = meinGrab.transform.position; ;
        meinGrab.Glimmt = true;
    }

    void BlickRichtung()
    {
        if (_rb2d.velocity == Vector2.zero && AnimationsStatus != EnumAnimationsStatus.Idle)
        {
            _animator.SetFloat("Speed", 0);
            AnimationsStatus = EnumAnimationsStatus.Idle;
            return;
        }

        if (_rb2d.velocity == Vector2.zero && AnimationsStatus == EnumAnimationsStatus.Idle)
        {
            return;
        }

        var x = _rb2d.velocity.x;
        var y = _rb2d.velocity.y;

        _animator.SetFloat("Speed", 1);

        if (Mathf.Abs(x) > Mathf.Abs(y))
        {
            if (x > 0 && AnimationsStatus != EnumAnimationsStatus.Rechts)
            {
                AnimationsStatus = EnumAnimationsStatus.Rechts;
                _animator.SetTrigger("Rechts");
                return;
            }
            if (x < 0 && AnimationsStatus != EnumAnimationsStatus.Links)
            {
                AnimationsStatus = EnumAnimationsStatus.Links;
                _animator.SetTrigger("Links");
                return;
            }
        }
        else
        {
            if (y > 0 && AnimationsStatus != EnumAnimationsStatus.Hoch)
            {
                AnimationsStatus = EnumAnimationsStatus.Hoch;
                _animator.SetTrigger("Hoch");
                return;
            }
            if (y < 0 && AnimationsStatus != EnumAnimationsStatus.Runter)
            {
                AnimationsStatus = EnumAnimationsStatus.Runter;
                _animator.SetTrigger("Runter");
                return;
            }
        }
    }
}
