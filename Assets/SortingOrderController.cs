﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingOrderController : MonoBehaviour {

	void Start () 
	{
        _renderer = GetComponent<SpriteRenderer>();

        _renderer.sortingOrder = (int)(transform.position.y * -100f);
	}

    public bool NurStart;

    private SpriteRenderer _renderer;

    void Update () 
	{
        if (NurStart)
            return;

        _renderer.sortingOrder = (int)(transform.position.y * -100f);
    }
}
