﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpielerAktion  {

    bool IstAktiv { get; set; }
}
