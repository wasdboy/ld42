﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlutController : MonoBehaviour {

    private void Start()
    {
        _glimmTimer = 0;
        _animator = GetComponent<Animator>();
    }

    public GrabController MeinGrab;

    private Animator _animator;

    private float _glimmTimer;
    private float _maximalerGlimmTimer = 4f;

    private void Update()
    {
        if (_glimmTimer < _maximalerGlimmTimer)
            _glimmTimer += Time.deltaTime;

        _animator.speed = 1f + (_glimmTimer / 5f);

        if (_glimmTimer >= _maximalerGlimmTimer)
        {
            MeinGrab.FeuerAn();           
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _glimmTimer = 0;
        AudioAdapter.Singleton.FeuerLoeschenSoundAbspielen();
        MeinGrab.Glimmt = false;
        gameObject.SetActive(false);
    }
}
