﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SkillUnlockPageController : MonoBehaviour {

    public Text NameText, BeschreibungText;
    public Image SkillImage;

    public GameObject Button;

    public GameObject GameOverController;

    private Action Click;

    public void OnClick()
    {
        Click();
    }

    public void ZeigePanel(NeuerSkillInfo info)
    {
        NameText.text = info.SkillName;
        BeschreibungText.text = info.SkillBeschreibung;
        SkillImage.sprite= info.SkillSprite;

        Click = () => { GameOverController.SetActive(true); Destroy(gameObject); };

        if (KonsitenteDaten.Singleton.NeueSkillInfos.Count <= 0)
            return;
        var naechsterSkill = KonsitenteDaten.Singleton.NeueSkillInfos.First();
        KonsitenteDaten.Singleton.NeueSkillInfos.RemoveAt(0);

        Click =() => { ZeigePanel(naechsterSkill); };
    }
}
