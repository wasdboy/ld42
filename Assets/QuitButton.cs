﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour {

    private void Start()
    {
#if UNITY_WEBGL
        gameObject.SetActive(false);
#endif
    }

    public Image Image;
    public Text Text;

    public void OnEnter()
    {
        Image.color = Color.red;
        Text.color = Color.red;
    }

    public void OnLeave()
    {
        Image.color = Color.white;
        Text.color = Color.white;
    }

    public void OnClick()
    {
        KonsitenteDaten.Singleton.Speichern();

        Application.Quit();
#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isPlaying)
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
#endif
    }
}
