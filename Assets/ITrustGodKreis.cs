﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ITrustGodKreis : MonoBehaviour {

    public SpielerAnimationsHelper Helper;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var zombie = collision.gameObject.GetComponent<ZombieController>();
        if (zombie == null)
            return;

        Helper.ZombieHinzufuegen(zombie);
    }
}
