﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeichenController : MonoBehaviour, ISpielerAktion {

    private void Start()
    {
        _hitBox = GetComponent<BoxCollider2D>();
        _rb2D = GetComponent<Rigidbody2D>();

        _LeichenSpriteRenderer.sprite = Sprite2;
    }

    public bool IstAktiv { get; set; }
    public bool IstVerfault { get { return _istVerfault; } }
    public bool IstBegraben { get { return _meinGrab != null; } }

    public SpriteRenderer _LeichenSpriteRenderer;
    public Animator LeichenAnimator;
    public ZombieController ZombiePrefab;
    public Sprite Sprite2;

    private bool _istAmTragen;
    private float _verfaulTimer;
    private float _maximalVerfaulTimer = 25f;

    private float _erloesungsTimer;
    private float _maximalErloesungsTimer = 60;

    private bool _istBegraben;
    private bool _blinkenWarnung;
    private bool _istVerfault;
    private bool _istAmFaulen;

    private GrabController _meinGrab;
    private BoxCollider2D _hitBox;
    private Rigidbody2D _rb2D;

    private void Update()
    {
        if (IstAktiv && SpielerController.Singleton.Status == SpielerController.EnumSpielerStatus.FreiFuerAktion && Input.GetKeyDown(KeyCode.Space))
            StarteTragen();

        if (_istAmTragen)
        {
            _rb2D.velocity = SpielerController.Singleton.Velocity;

            var x = transform.position.x;
            var y = transform.position.y;

            if (transform.position.x < SpielerController.Singleton.Links)
                x = SpielerController.Singleton.Links;
            if (transform.position.x > SpielerController.Singleton.Rechts)
                x = SpielerController.Singleton.Rechts;
            if (transform.position.y < SpielerController.Singleton.Unten)
                y = SpielerController.Singleton.Unten;
            if (transform.position.y > SpielerController.Singleton.Oben)
                y = SpielerController.Singleton.Oben;

            transform.position = new Vector2(x, y);
        }

        if (_istAmTragen && Input.GetKeyUp(KeyCode.Space))
            StoppeTragen();

        if (_istBegraben == false && _verfaulTimer < _maximalVerfaulTimer)
            _verfaulTimer += Time.deltaTime;

        if (_blinkenWarnung == false && (_verfaulTimer / _maximalVerfaulTimer) >= 0.5f)
        {
            LeichenAnimator.SetTrigger("BlinkenGelb");
            _blinkenWarnung = true;
        }

        if (_istAmFaulen == false && (_verfaulTimer / _maximalVerfaulTimer) >= 0.9f)
        {
            _istAmFaulen = true;

            LeichenAnimator.SetTrigger("BlinkenRot");
        }

        if (_istVerfault == false && _verfaulTimer >= _maximalVerfaulTimer)
        {
            _hitBox.enabled = false;
            _istVerfault = true;

            if (_istAmTragen)
                StoppeTragen();

            if (_meinGrab != null)
                _meinGrab.Leiche = null;

            var zombie = Instantiate(ZombiePrefab, transform.position, Quaternion.identity);

            LeichenSpawner.NeuerZombie(zombie);

            Destroy(gameObject);
        }

        if (_istBegraben && _istVerfault == false && _erloesungsTimer < _maximalErloesungsTimer)
            _erloesungsTimer += Time.deltaTime;

        if (_istBegraben && _istVerfault == false && _erloesungsTimer >= _maximalErloesungsTimer)
            ErloesenImGrab();
    }

    void StarteTragen()
    {
        _istAmTragen = true;
        SpielerController.Singleton.SetBewegungTragend();
        SpielerController.Singleton.LeichenController = this;
        _LeichenSpriteRenderer.sortingLayerName = "Entities";
        _LeichenSpriteRenderer.sortingOrder = 2000;
    }

    public void StoppeTragen()
    {
        _istAmTragen = false;
        SpielerController.Singleton.SetBewegungNormal();
        SpielerController.Singleton.LeichenController = null;
        _LeichenSpriteRenderer.sortingLayerName = "Grab";
        _LeichenSpriteRenderer.sortingOrder = 1;
        _rb2D.velocity = Vector2.zero;
    }

    void GeheInsGrab()
    {
        _hitBox.enabled = false;
        StoppeTragen();
        transform.position = (Vector2)_meinGrab.transform.position;
        transform.localScale = new Vector3(0.7f, 0.7f, 1);

        _LeichenSpriteRenderer.sortingLayerName = "Grab";
        _LeichenSpriteRenderer.sortingOrder = 5;

        _meinGrab.Leiche = this;
        _meinGrab.ResetAktionen();

        AudioAdapter.Singleton.GrabenSoundAbspielen();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var grab = collision.gameObject.GetComponent<GrabController>();

        if (grab == null)
            return;

        _meinGrab = grab;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_meinGrab == null)
            return;

        if (_meinGrab.Status == GrabController.EnumGrabStatus.Offen && _meinGrab.Leiche == null)
            GeheInsGrab();
    }

    public void ErloesenImGrab()
    {
        _meinGrab.GrabAnimator.SetTrigger("Engel");
        _meinGrab.Leiche = null;
        AudioAdapter.Singleton.EngelSoundAbspielen();
        Destroy(gameObject);
    }

    public void Erloesen()
    {
        LeichenAnimator.SetTrigger("Erloesen");
    }

    public void Erloest()
    {
        SpielerController.Singleton.AnzahlEngel++;
        AudioAdapter.Singleton.EngelSoundAbspielen();
        Destroy(gameObject);
    }

    public void SetVisibility(bool status)
    {
        _LeichenSpriteRenderer.enabled = status;
    }

    public void SetBegraben()
    {
        _istBegraben = true;
    }

    public void SetVerfault(bool wert)
    {
        _istVerfault = wert;
    }
}
