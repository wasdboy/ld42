﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabController : MonoBehaviour,ISpielerAktion {

    private void Start()
    {
        _anzahlAktionen = 0;
        _collider = GetComponent<BoxCollider2D>();
        Status = EnumGrabStatus.Geschlossen;
        GlutController.MeinGrab = this;
    }

    public enum EnumGrabStatus { Offen, Geschlossen,Zerstoert}
    public EnumGrabStatus Status;

    public bool IstAktiv { get; set; }
    public bool Glimmt;

    public Sprite GrabOffenSprite;
    public Sprite GrabGeschlossenSprite;
    public SpriteRenderer GrabSpriteRenderer;
    public LeichenController Leiche;
    public ZombieController ZombiePrefab;
    public Animator GrabAnimator;
    public GameObject Feuer;
    public Transform GrabFlaecheTransform;
    public ParticleSystem ParticleSystem;
    public GlutController GlutController;

    private BoxCollider2D _collider;
    private int _anzahlAktionen;
    private int _maximaleAktionen = 1;
    private bool _wurdeMitLeicheGeoeffnet;

    void Update () 
	{
        if (IstAktiv == false || SpielerController.Singleton.Status != SpielerController.EnumSpielerStatus.FreiFuerAktion || Status ==  EnumGrabStatus.Zerstoert)
            return;

        if (Input.GetKeyDown(KeyCode.Space))
            AktionAusfuehren();
	}

    void AktionAusfuehren()
    {
        SpielerController.Singleton.Grabe(GrabFlaecheTransform);
        if (Status == EnumGrabStatus.Geschlossen)
            Aufgraben();
        else if (Status == EnumGrabStatus.Offen)
            Zugraben();
    }

    void Aufgraben()
    {
        _anzahlAktionen++;

        ParticleSystem.Emit(UnityEngine.Random.Range(5, 10));

        if (_anzahlAktionen < _maximaleAktionen)
        {
            AudioAdapter.Singleton.GrabenSoundAbspielen();
            return;
        }

        AudioAdapter.Singleton.GrabenFertigSoundAbspielen();
        GrabOeffnen();
    }

    public void GrabOeffnenSkill()
    {
        ParticleSystem.Emit(UnityEngine.Random.Range(10, 20));
        GrabOeffnen();
    }

    private void GrabOeffnen()
    {
        _anzahlAktionen = 0;
        Status = EnumGrabStatus.Offen;
        
        GrabSpriteRenderer.sprite = GrabOffenSprite;

        if (Leiche == null)
            return;

        Leiche.SetVisibility(true);
        Leiche.SetVerfault(true);
        GrabAnimator.SetTrigger("Teufel");
        _wurdeMitLeicheGeoeffnet = true;
        AudioAdapter.Singleton.TeufelSoundAbspielen();
        _collider.enabled = false;
    }

    public void GlutLegen()
    {
        GlutController.gameObject.SetActive(true);
    }

    public void FeuerAn()
    {
        if(Leiche != null)
            Destroy(Leiche.gameObject);

        AudioAdapter.Singleton.FeuerZuGlutSoundAbspielen();

        GrabSpriteRenderer.sprite = GrabOffenSprite;
        Feuer.SetActive(true);
        Status = EnumGrabStatus.Zerstoert;
    }

    public void ZombieSpawnen()
    {
        var zombie = Instantiate(ZombiePrefab, transform.position, Quaternion.identity);
        LeichenSpawner.NeuerZombieRot(zombie);
    }

    public void EngelInHimmel()
    {
        AudioAdapter.Singleton.EngelErloestSoundAbspielen();
        SpielerController.Singleton.AnzahlEngel++;
    }

    void Zugraben()
    {
        if (_wurdeMitLeicheGeoeffnet || (Leiche != null && Leiche.IstVerfault))
            return;

        _anzahlAktionen++;
        
        if (_anzahlAktionen < _maximaleAktionen)
        {
            AudioAdapter.Singleton.GrabenSoundAbspielen();
            return;
        }

        _anzahlAktionen = 0;
        Status = EnumGrabStatus.Geschlossen;
        AudioAdapter.Singleton.GrabenFertigSoundAbspielen();
        GrabSpriteRenderer.sprite = GrabGeschlossenSprite;

        if (Leiche == null)
            return;

        Leiche.SetVisibility(false);
        Leiche.SetBegraben();

        if(Leiche.IstVerfault == false)
            Leiche.LeichenAnimator.SetTrigger("Idle");
    }

    public void ResetAktionen()
    {
        _anzahlAktionen = 0;
    }
}
