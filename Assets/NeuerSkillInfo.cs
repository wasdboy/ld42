﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class NeuerSkillInfo {

    public string SkillName;
    public string SkillBeschreibung;
    public Sprite SkillSprite;
}
